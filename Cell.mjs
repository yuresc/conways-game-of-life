export class Cell {
    constructor(status) {
        this.isLiving = status;
    };

    toggle() {
        this.isLiving = !this.isLiving
    }
};