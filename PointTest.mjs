import { Point } from "./Point.mjs";

export const PointTestStart = () => {
    if (Point.asIndex(0, 0) !== 0) {
        console.log(`Cannot convert point to index.`)
    }
    else if (Point.asIndex(12, 10) !== 142) {
        console.log(`Expected 142, recieved ${Point.asIndex(12, 10)}`)
    }
    else if (Point.asIndex(7, 3) !== 46) {
        console.log(`Expected 43, recieved ${Point.asIndex(7, 3)}`)
    }
    else {
        console.log('No errors found in Point class')
    }

}