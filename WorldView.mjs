export class WorldView {
    static renderHTML(world) {
        let worldString = ``;
        worldString += `<h3 style = "font-family: 'Courier New';"> Generation ${world.generation}`;
        for (let i = 0; i < world._grid.length; i++) {
            if (i % world.width === 0) {
                worldString += `<br/>`
            }
            worldString += world._grid[i].isLiving ? 'O' : '.'
        }
        worldString += '</h1>'
        return worldString;
    }
}