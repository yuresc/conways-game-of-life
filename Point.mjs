import { settings } from "./Settings.mjs";
export class Point {
    x;
    y;

    constructor(x, y) {
        this.x = Point.adjustXForSphericalWorld(x);
        this.y = Point.adjustYForSphericalWorld(y);
    }

    static adjustYForSphericalWorld(y) {
        if (y < 0) {
            y += settings.world.length
        }
        else if (y > settings.world.length - 1) {
            y -= settings.world.length
        }
        return y
    }
    static adjustXForSphericalWorld(x) {
        if (x < 0) {
            x += settings.world.width
        }
        else if (x > settings.world.width - 1) {
            x -= settings.world.width
        }
        return x
    }

    static fromIndex(index) {
        let x = index % settings.world.width;
        let y = (index - x) / settings.world.width;

        return new Point(x, y);
    }

    static asIndex(x, y) {
        let index = x + (y * settings.world.width);
        return index
    }
}