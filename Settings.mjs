export const settings = {
    world: {
        width: 100,
        length: 100,
    },
    display: {
        widthScale: 10,
        lengthScale: 10
    }
}