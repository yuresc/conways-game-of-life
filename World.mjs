import { settings } from "./Settings.mjs";
import { Cell } from "./Cell.mjs";
import { Point } from "./Point.mjs";
export class World {
    _grid;
    generation;
    length;
    width;

    constructor() {
        this.generation = 0;
        this.length = settings.world.length
        this.width = settings.world.width
        this._grid = Array(this.length * this.width).fill().map(x => x = new Cell(false));

    }

    toggleCell = (x, y) => {
        let cell = this.getCellFromPoint(new Point(x, y));
        cell.toggle();
    }

    getCellFromIndex = (index) => {
        return this._grid[index];
    }
    getCellFromPoint = (point) => {
        return this.getCellFromIndex(Point.asIndex(point.x, point.y))
    }
    getPointsOfNeighbors = (x, y) => {
        let neighborCoordinates = [
            new Point(x - 1, y + 1),
            new Point(x, y + 1),
            new Point(x + 1, y + 1),
            new Point(x - 1, y),
            new Point(x + 1, y),
            new Point(x - 1, y - 1),
            new Point(x, y - 1),
            new Point(x + 1, y - 1)
        ]
        return neighborCoordinates
    }
    // getIndexesOfNeighbors = (x, y) => {
    //     let neighbors = [
    //         Point.asIndex(x - 1, y + 1),
    //         Point.asIndex(x, y + 1),
    //         Point.asIndex(x + 1, y + 1),
    //         Point.asIndex(x - 1, y),
    //         Point.asIndex(x + 1, y),
    //         Point.asIndex(x - 1, y - 1),
    //         Point.asIndex(x, y - 1),
    //         Point.asIndex(x + 1, y - 1)
    //     ]
    //     return neighbors
    // }

    getNeighborCells = (x, y) => {
        let neighbors = []
        this.getPointsOfNeighbors(x, y).map((point) => {neighbors.push(this.getCellFromPoint(point))})
        return neighbors;
    }

    getNeighborCellsStatus = (x , y) => {
        let neighbors = []
        this.getPointsOfNeighbors(x, y).map(
            (point) => {
                neighbors.push(this.getCellFromPoint(point).isLiving)})
        return neighbors;
    }

}