import { WorldView } from "./WorldView.mjs";
import { Point } from "./Point.mjs"
import { World } from "./World.mjs";
export class WorldController {
    _world;
    generation;

    constructor(world) {
        this._world = world;
        this.generation = 0;
    }

    shiftOneGeneration = () => {
        this._world = this.getNextGeneration()
        this.generation += 1;
        this._world.generation = this.generation;
        return this._world
    }

    getNextGeneration() {
        let world =  new World();
        this._world._grid.forEach((cell, index) => {
            let point = Point.fromIndex(index)
            world._grid[index].isLiving = this.shouldCellLive(point.x, point.y);
        })


        return world
    }

    shouldCellLive = (x, y) => {
        let liveNeighbors = 0;
        this._world.getNeighborCellsStatus(x, y).forEach((status) => {
            status ? liveNeighbors++ : null
        })
        if (this._world.getCellFromPoint(new Point(x, y)).isLiving) {
            if (liveNeighbors === 3 || liveNeighbors === 2) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (liveNeighbors === 3) {
                return true;
            }
            return false;
        }
    }

    getWorldView = () => {
        return WorldView.renderHTML(this._world);
    }
}